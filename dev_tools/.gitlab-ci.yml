stages:
    - build
    - test
    - deploy

image: registry.gitlab.com/quantum-computing-software/tools/development_environment

variables:
    GIT_SUBMODULE_STRATEGY: normal
    CI_ENV_DIR: "$CI_PROJECT_DIR/envs/ci"
    CONDA_PKGS_DIRS: "$CI_PROJECT_DIR/.conda-pkgs-cache/"
    TEST_VERSION: "false"
    TEST_CONDA_BUILD: "false"
    TEST_METADATA: "false"
    CREATE_DOCS: "false"
    TEST_ALL: "false"

before_script:
    - source ~/.profile  # make conda available

workflow:  # to deal with branch pipelines and merge request pipelines
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH


environment:
    stage: build
    script:
        - bash run setup --env_dir $CI_ENV_DIR
    artifacts:
        when: on_success
        expire_in: 1 days
        paths:
            - $CI_ENV_DIR
    cache:
        key: conda-cache  # cache the conda package downloads
        paths:
            - $CONDA_PKGS_DIRS/*.conda
            - $CONDA_PKGS_DIRS/*.tar.bz2
            - $CONDA_PKGS_DIRS/urls*
            - $CONDA_PKGS_DIRS/cache


tests:
    stage: test
    script:
        - conda activate $CI_ENV_DIR
        - bash run check pytest tests.txt
    artifacts:
        untracked: false
        expire_in: 1 days
        paths:
            - tests.txt

coverage:
    stage: test
    script:
        - conda activate $CI_ENV_DIR
        - bash run check coverage coverage.txt
    coverage: /TOTAL.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/
    artifacts:
        untracked: false
        expire_in: 1 days
        paths:
            - coverage.txt

style:
    stage: test
    script:
        - conda activate $CI_ENV_DIR
        - bash run check pylint pylint.txt
    artifacts:
        untracked: false
        expire_in: 1 days
        paths:
            - pylint.txt

tutorials:
    stage: test
    script:
        - conda activate $CI_ENV_DIR
        - bash run check tutorials

conda_build:
    stage: test
    script:
        - conda activate $CI_ENV_DIR
        - bash run build_conda
    rules:
        - if: $TEST_CONDA_BUILD == "true" || $TEST_ALL == "true"     # via push variable
        - if: $CI_COMMIT_BRANCH == "development"                     # direct push on development
        - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "development"  # merge to development
        - if: $CI_COMMIT_BRANCH || $CI_OPEN_MERGE_REQUESTS           # on any branch or MR if metadata file is changed
          changes:
            - dev_tools/conda_build_recipe/meta.yaml

metadata:
    stage: test
    script:
        - conda activate $CI_ENV_DIR
        - bash run check_metadata keep metadata/
    rules:
        - if: $TEST_METADATA == "true" || $TEST_ALL == "true"  # via push variables
        - if: $CI_COMMIT_BRANCH || $CI_OPEN_MERGE_REQUESTS     # on any branch or MR if metadata files are changed
          changes:
              - METADATA.yaml
              - CITATION.cff
              - README.md
              - setup.py
              - requirements.txt
              - dev_tools/conda_build_recipe/meta.yaml
    artifacts:
        untracked: false
        when: on_failure
        expire_in: 1 days
        paths:
            - metadata/

version:
    stage: test
    script:
        - source run check_version
        - echo "VERSION=$VERSION" >> variables.env  # add version and others to the variables.env file
        - echo "CHANNEL=$CHANNEL" >> variables.env
        - echo "PACKAGE=$PACKAGE" >> variables.env
    artifacts:
        reports:
            dotenv: variables.env  # expose the variables to other jobs
    rules:
        - if: $TEST_VERSION == "true" || $TEST_ALL == "true"  # via push variables
        - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"  # always when we want to merge to main
                                                              # (no direct pushs to main are allowed nevertheless)
        - if: $CI_COMMIT_BRANCH == "main"  # direct push to main for release


pages:
    stage: deploy
    script:
        - conda activate $CI_ENV_DIR
        - bash run create_docs public/
    artifacts:
        paths:
            - public/
    rules:
        - if: $CREATE_DOCS == "true"       # via push variable
        - if: $CI_COMMIT_BRANCH == "main"  # on main

conda_release:
    stage: deploy
    script:
        - conda activate $CI_ENV_DIR
        - bash run build_conda $ANACONDA_API_TOKEN
    rules:
        - if: $CI_COMMIT_BRANCH == "main"  # only on main

gitlab_release:
    stage: deploy
    needs:
        - job: version
          artifacts: true
    image: registry.gitlab.com/gitlab-org/release-cli:latest
    rules:
        # - if: '$CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/'
        - if: $CI_COMMIT_TAG
          when: never  # do not run this job when a tag is created manually
        - if: $CI_COMMIT_BRANCH == "main"  # only on main
    before_script:  # overwrite the default before script
        - echo "start release"
    script:
        - echo "release $VERSION"
    release:
        tag_name: "v$VERSION"
        tag_message: "version $VERSION"
        name: "Package release $VERSION"
        description: modules/development_tools/build/release_description.md
        ref: "$CI_COMMIT_SHA"  # release is created in the pipeline of this commit
        assets:
            links:
                - name: "$PACKAGE on Anaconda channel $CHANNEL"
                  url: "https://anaconda.org/$CHANNEL/$PACKAGE"
                  link_type: "package"
                - name: "Documentation of $PACKAGE"
                  url: "https://quantum-computing-software.gitlab.io/$PACKAGE/"
