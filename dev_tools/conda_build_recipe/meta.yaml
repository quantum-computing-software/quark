package:
  name: quark
  version: '1.2'
  released: '2025-02-21'
  doi: 10.5281/zenodo.13944213
about:
  title: quark - QUantum Application Reformulation Kernel
  home: https://gitlab.com/quantum-computing-software/quark/
  license: Apache-2.0
  license_family: APACHE
  license_file: LICENSE
  summary: This is a software package to support the mapping of combinatorial optimization
    problems to quantum computing interfaces via QUBO and Ising problems.
  keywords:
  - Combinatorial Optimization
  - QUBO
  - Ising Problem
  - Quantum Computing
  - Quantum Annealing
  doc_url: https://quantum-computing-software.gitlab.io/quark/
  dev_url: https://gitlab.com/quantum-computing-software/quark/
  description: '# Description of the Basic Ideas


    The combinatorial optimization problem is rewritten as a single (quadratic unconstrained
    binary) objective function.

    The usual way to build it up is to use the following structure:

    In the **`Instance`** we describe the problem defining parameters.

    From the instance, we construct the **`ObjectiveTerms`**,

    containing the different contributions to the objective function,

    in particular the ones derived from problem constraints.

    The objective terms can be implemented directly or derived from a **`ConstrainedObjective`**,

    which contains the objective function and multiple constraints, implemented as
    **`ConstraintBinary`**.

    The objective terms can now be used to create the **`Objective`**

    by summing up the single terms weighted with a certain so-called penalty weight.


    All objective objects contain **`Polynomials`** representing the functions.

    There are special polynomials, **`PolyBinary`** and **`PolyIsing`**,

    which take advantage of the restriction to either binary (0 or 1) or spin (-1
    or 1) variables.


    The **`ScipModel`** is an interface to the classical MILP solver [SCIP](https://scip.zib.de/),

    which can solve a **`ConstrainedObjective`** or a (small enough) **`Objective`**
    for comparison.

    In **`Solution`**, we store not only the optimal variable assignment but also
    further information,

    like runtime etc., which are obtained during the solving process.


    Furthermore, we have the **`HardwareAdjacency`** and the **`Embedding`**,

    which are useful when dealing with actual hardware.


    All mentioned objects also provide methods to store and load their information
    in and from hdf5 files.'
author:
  name: DLR-SC
  alias: https://gitlab.com/quantum-computing-software/quark/-/blob/development/CONTRIBUTORS
  website: https://www.dlr.de/en/sc/research-transfer/research-topics/high-performance-computing-and-quantum-computing
  affiliation: German Aerospace Center (DLR) - Institute of Software Technology (SC)
  email: qc-software@dlr.de
source:
  path: ../../
extra:
  maintainers:
  - elloline
  - l.windgaetter
requirements:
  build:
  - python
  - setuptools
  host:
  - python
  - pip
  run:
  - python>=3.11
  - h5py
  - scip
  - pyscipopt
  - numpy<=1.26
  - pytest
  - networkx
  - bidict
  - matplotlib
  - jupyter
build:
  number: 1
  noarch: python
  script: '{{ PYTHON }} -m pip install . -vv'
test:
  import:
  - quark
  requires:
  - pytest
  source_files:
  - tests/test_*.py
  - tests/io/test_*.py
  - tests/utils/test_*.py
  commands:
  - python tests/test_*.py tests/io/test_*.py tests/utils/test_*.py
packages:
- quark
- quark.io
- quark.testing
- quark.utils
