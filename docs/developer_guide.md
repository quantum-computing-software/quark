# Developer Guide

Here you find more information on how to work with and extend our package quark, 
whose source code can be found [here](https://gitlab.com/quantum-computing-software/quark).


## Repository Structure

`quark/`
* `quark/` - the actual source code folder including (abstract) base classes
    * `io/` - contains all input/output dedicated functionality as it is separated from the actual functionality
    * `utils/` - helper classes to use the quark library
    * `testing/` - example implementations of the abstract base classes showing usage of library
* `dev_tools/`
    *  `conda_build_recipe/`
        * `meta.yaml` - metadata file for conda build (automatically generated!)
    * `.gitlab-ci.yml` - definition of the gitlab CI
    * `settings.sh` - local settings overwriting the ones from `development_tools` module
* `docs/`
    * `tutorials/` - jupyter notebooks showing usage of quark 
    * `src/` - source files for documentation
    * other files explaining the usage of quark
* `modules/`
    * `development_tools/` - git submodule containing tools for development
* `tests/` - contains the tests, which are also good examples on usage of objects
    * `io/` - tests dedicated to IO
    * `utils/` - tests dedicated to helper classes
    * `testdata/` - fixed test files used in the tests
* `CITATION.cff` - data for citing this package (automatically generated!)
* `CONTRIBUTORS` - list of all people who have contributed to the package
* `LICENSE` - the license file
* `METADATA.yaml` - single point of truth for metadata information
* `README.md` - first entrance point at repository (automatically generated!)
* `run` - script combining several individual scripts from the development tools, 
          e.g. to set up development environment, run the checks, create metadata etc. 
          (can only be used, if development_tools are initialized)
* `requirements.txt` - conda requirements
* `setup.py` - for build (automatically generated!)


## Setting up the development environment

by using our [development_tools](https://gitlab.com/quantum-computing-software/tools/development_tools)

### Linux

0. Check if **Git** is installed, otherwise do 
   ```
   sudo apt-get update
   sudo apt install git
   ``` 
   and clone this repository.

1. Use [development_environment](https://gitlab.com/quantum-computing-software/tools/development_environment) 
   
   :information_source: This will install **miniforge3** (a fork of (Mini-/Ana-)**conda**) and other necessary packages 
   to your local machine. 

   Check if conda is available by typing `conda` in some terminal.
   If it cannot find the command you need to execute
   ```
   echo ". ~/programs/install/miniforge3/etc/profile.d/conda.sh" >> ~/.bashrc
   ``` 
   (resp. other path, if you have changed the default path)
   to make it globally available. 
   Close and reopen the terminal and check it again.  

2. Create a **virtual environment**

   :information_source: It is best practice to use virtual environments, for further information see 
   [Conda Docs](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#).
      
   Make sure the git submodules are initialized by 
   ```
   git submodule update --init
   ```   
      
   To create a virtual environment using our tools open a terminal in the quark folder and execute 
    ```
    bash run setup [local] [--env_name <nondefault_env_name>]
    ```
   For more details and options see in the module [development_tools](https://gitlab.com/quantum-computing-software/tools/development_tools).  

3. Activate the environment with 
   ```
   conda activate env_name
   ```
   with the corresponding environment name, respectively, 
   with `envs/env_name` if the local flag was used.  
   
4. Test if all went fine by executing the checks described in the following section.

### Windows

The steps correspond to the ones from the Linux installation but the preparation of the system needs to be done manually.
**Git** should already be installed as well as **Miniforge**. 
   
Afterwards you can follow the steps 2. \- 4. of the Linux setup but using the **Miniforge Prompt** or **Git bash** instead of the Linux terminal.

If your preferred shell is e.g. **Git bash**, it might be necessary to execute
```
conda init bash
```
beforehand to activate the full functionality of conda.

(It is recommended to use the local flag for environments as you probably won't have access to the conda installation repository without admin rights.
If you use **PyCharm** the `envs/` folder can cause strange warning, thus it needs to be marked as excluded.)
      

## Testing the code

We have several code quality checks which can be run with our 
[development_tools](https://gitlab.com/quantum-computing-software/tools/development_tools) by
```
bash run check [pytest] [coverage] [pylint] [tutorials]
```
with the options
* `pytest` - to see whether the tests run through,
* `coverage` - to see whether the code coverage is sufficient,
* `pylint` - to see if the style is compliant with our regulations, or
* `tutorials`- to run the example jupyter notebooks.

These checks are also used in our continuous integration, 
which will be run every time you push something to this repository. 


## Best Practices for Developers

### Workflow

* Recognizing a bug or missing feature
* Create issue
  + give possibility to discuss it
  + if urgent ask someone directly or add/mention someone explicitly
  + agree on content and approach of issue
  + if necessary adjust title and description!
* Create branch and merge request from selected issue
  + use gitlab infrastructure
    - follows naming conventions automatically
    - keeps track of your changes
  + good for getting feedback already during programming process
* Work on issue in the merge request
  + keep WIP/draft status
  + if minor questions or problems arise leave comment in merge request
    - if necessary mention someone explicitly with '@username'
    - can also be used to take notes or structure work subtasks with tick boxes 
  + if you recognize another bug or missing feature → create new issue
  + always include meaningful tests to check your code
  + commit code to repository with meaningful messages
* Assign someone else to have a look at your code
  + if you think you made already a big progress or
  + if you run into wholes and need help
  + wait until you are reassigned
  + probably discuss options
* Iterate last two steps if necessary
* Finalize
  + resolve all remarks of the reviewer 
  + make sure the tests run, and you fulfil the required coverage and pylint scores!
  + add yourself to the CONTRIBUTORS file
  + if merge conflicts might appear merge development into your branch 
* Done
  + resolve WIP status
  + assign to reviewer
  + wait for approval and merge


## Automated local checks 

It is possible to enforce automated local checks locally after every commit using git hooks.
The related scripts can be found in the dev_tools/git folder and executed with
```
bash modules/development_tools/git/add_hooks.sh
```
It is still possible to commit without using the hooks by
```
git commit --no-verify
```

The hooks can be removed with 
```
bash modules/development_tools/git/remove_hooks.sh
```
