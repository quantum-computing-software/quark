
.. toctree::
    :maxdepth: 2
    :caption: First Steps

    description.md
    installation.md
    getting_started.md

.. toctree::
    :maxdepth: 1
    :caption: Tutorials

    tutorials/Tutorial 01 - How to use Polynomials.ipynb
    tutorials/Tutorial 02 - How to formulate QUBOs by hand using Polynomials.ipynb
    tutorials/Tutorial 03 - How to reuse the QUBO formulation.ipynb
    tutorials/Tutorial 04 - How to handle constraints automatically.ipynb
    tutorials/Tutorial 05 - How to solve QUBOs.ipynb
    tutorials/Tutorial 06 - How to store intermediate data.ipynb
    tutorials/Tutorial 07 - How to connect to D-Wave.ipynb
    tutorials/Tutorial 08 - How to connect to QAOA simulator.ipynb
    tutorials/Tutorial 09 - How to connect to the PySA solver.ipynb

.. toctree::
    :maxdepth: 1
    :caption: Reference

    reference.rst

.. toctree::
    :maxdepth: 1
    :caption: Contribute

    developer_guide.md
