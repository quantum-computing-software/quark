{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f4c6fe1f",
   "metadata": {},
   "source": [
    "# Tutorial 01 - How to use Polynomials"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "3ebd9367",
   "metadata": {},
   "outputs": [],
   "source": [
    "from quark import Polynomial"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38287fa8",
   "metadata": {},
   "source": [
    "## What we will learn\n",
    "\n",
    "In this tutorial, we will learn\n",
    "1. how to use the ``Polynomial`` and\n",
    "2. how to use ``PolyBinary`` and ``PolyIsing`` correspondingly."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "054abad5",
   "metadata": {},
   "source": [
    "## 1. The Polynomial class"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "98e09f3a",
   "metadata": {},
   "source": [
    "### Representation as dicts\n",
    "\n",
    "The main object which we need to for the optimization problem formulation are polynomials. We can store a polynomial like this\n",
    "\n",
    "$$ 3 x_1 - y_3 - 2 x_1 y_3 + 5 y_3^2 $$\n",
    "\n",
    "where $x_1$ and $y_3$ are some variables, by storing its coefficients:\n",
    "\n",
    "| coefficient of   | coefficient value |\n",
    "|------------------|-------------------|\n",
    "|        $x_1$     |        3          |\n",
    "|        $y_3$     |        -1         |\n",
    "|        $x_1 y_3$ |        -2         |\n",
    "|        $y_3^2$   |        5          |\n",
    "\n",
    "We can do this in a dictionary, where the keys are tuples representing the variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "7148d339",
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_as_dict = {(('x', 1),): 3,\n",
    "                (('y', 3),): -1,\n",
    "                (('x', 1), ('y', 3)): -2,\n",
    "                (('y', 3), ('y', 3)): 5}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b44cc5e",
   "metadata": {},
   "source": [
    "To use some neat convenience functions like multiplication and addition we use the polynomial class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "4eee8658",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Polynomial({(('x', 1),): 3, (('y', 3),): -1, (('x', 1), ('y', 3)): -2, (('y', 3), ('y', 3)): 5})"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly = Polynomial(poly_as_dict)\n",
    "poly"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "51570716",
   "metadata": {},
   "source": [
    "We have some nicer formatting:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "d5c23c65",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+3 x_1 -1 y_3 -2 x_1 y_3 +5 y_3 y_3\n"
     ]
    }
   ],
   "source": [
    "print(poly)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b063876",
   "metadata": {},
   "source": [
    "Now we can add, subtract or multiply polynomials"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "8cc0a5b4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-6 x_1 +2 y_3 +4 x_1 y_3 -10 y_3 y_3\n"
     ]
    }
   ],
   "source": [
    "new_poly = poly - 3 * poly\n",
    "print(new_poly)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a41c932b",
   "metadata": {},
   "source": [
    "### Different types of variables"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebc14c3f",
   "metadata": {},
   "source": [
    "In the above examples the variables are represented by tuples, e.g., `('x', 1)`. We also have the possibility to encode 'flat' variables, meaning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "ee346ec5",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Polynomial({(1,): 3, (3,): -1, (1, 1): -2, (1, 3): 5})"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly_as_dict_flat = {(1,): 3,\n",
    "                     (3,): -1,\n",
    "                     (1, 1): -2,\n",
    "                     (1, 3): 5}\n",
    "poly_flat = Polynomial(poly_as_dict_flat)\n",
    "poly_flat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "69965d20",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+3 x1 -1 x3 -2 x1 x1 +5 x1 x3\n"
     ]
    }
   ],
   "source": [
    "print(poly_flat)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5d624893",
   "metadata": {},
   "source": [
    "where the entries only represent the index of a variable. If the indices start at 0, we call the polynomial 'compact'.\n",
    "\n",
    "Note that we cannot mix both types of variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "a619ae32",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pytest\n",
    "with pytest.raises(TypeError, match=\"Expected variable type 'Integral', but got 'tuple'\"):\n",
    "    Polynomial({(1, 1) : -2, (('y', 3), ('y', 3)): 5.0})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "111f99aa",
   "metadata": {},
   "source": [
    "Remember the non-flat polynomial from above"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "d1a7ff8c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+3 x_1 -1 y_3 -2 x_1 y_3 +5 y_3 y_3\n"
     ]
    }
   ],
   "source": [
    "print(poly)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50c1ffbe",
   "metadata": {},
   "source": [
    "where its variables are given by"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "f731ccdc",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[('x', 1), ('y', 3)]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly.variables"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1f1e56f5",
   "metadata": {},
   "source": [
    "We can convert it to a non-flat polynomial with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "675167a6",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Polynomial({(0,): 3, (1,): -1, (0, 1): -2, (1, 1): 5})"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly_compact = poly.compact()\n",
    "poly_compact"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "465d4cdc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+3 x0 -1 x1 -2 x0 x1 +5 x1 x1\n"
     ]
    }
   ],
   "source": [
    "print(poly_compact)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a30b7350",
   "metadata": {},
   "source": [
    "where the above variable list defines the indexing according to the sorting."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d12d67f",
   "metadata": {},
   "source": [
    "### Other useful methods"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13036d2c",
   "metadata": {},
   "source": [
    "the degree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "5df6d41c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly.degree"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d667f382",
   "metadata": {},
   "source": [
    "the constant offset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "14c07aeb",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly.offset"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e3e0482c",
   "metadata": {},
   "source": [
    "linear part"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "89d392af",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{('x', 1): 3, ('y', 3): -1}"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly.linear_plain"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02c83121",
   "metadata": {},
   "source": [
    "quadratic part"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "21a60595",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Polynomial({(('x', 1), ('y', 3)): -2, (('y', 3), ('y', 3)): 5})"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly.quadratic"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46297402",
   "metadata": {},
   "source": [
    "evaluation on concrete variable assignment"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "06fb0a8e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly.evaluate({('x', 1): 1, ('y', 3): 0})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9633eceb",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "some more information on the coefficients"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "f864eca2",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'min_abs_coeff': 1,\n",
       " 'max_abs_coeff': 5,\n",
       " 'min_dist': 1,\n",
       " 'min_abs_dist': 1,\n",
       " 'max_min_ratio': 5.0,\n",
       " 'max_dist_ratio': 5.0,\n",
       " 'max_abs_dist_ratio': 5.0}"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly.coefficients_info"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02f7db67",
   "metadata": {},
   "source": [
    "## 2. PolyBinary and PolyIsing class\n",
    "\n",
    "Note that we did not give any limitation to what the variables $x_1$ and $y_3$ are. If $x_1, y_3 \\in \\{0, 1\\}$ for example $y_3^2 = y_3$.\n",
    "``Polynomial`` does not make use of the variable type of the polynomial. It could be anything.\n",
    "Now we want to introduce the derived classes of ``Polynomial`` which fix the variable type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "27eec874",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from quark import PolyBinary, PolyIsing"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5790ac6a",
   "metadata": {},
   "source": [
    "### PolyBinary\n",
    "\n",
    "First we have the polynomial with binary variables:\n",
    "\n",
    "``PolyBinary`` makes use of the fact that for  $x_i \\in \\{0, 1\\}$ we have\n",
    "\n",
    "$$ x_i^n = x_i \\qquad \\forall i \\quad \\forall n \\in \\mathbb{N}. $$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "86b9d460",
   "metadata": {},
   "source": [
    "As an example consider the polynomial"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "edabd94f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-1 x1 +1 x2 +2 x2 x2\n"
     ]
    }
   ],
   "source": [
    "poly = Polynomial({(2, 2): 2, (2,): 1, (1,): -1})\n",
    "print(poly)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac035a70",
   "metadata": {},
   "source": [
    "which reduces in the binary format to"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "58d67252",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-1 x1 +3 x2\n"
     ]
    }
   ],
   "source": [
    "poly_binary = PolyBinary({(2, 2): 2, (2,): 1, (1,): -1})\n",
    "print(poly_binary)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e17ddbb4",
   "metadata": {},
   "source": [
    "or the example from above"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "13e277b2",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "PolyBinary({(('x', 1),): 3, (('y', 3),): 4, (('x', 1), ('y', 3)): -2})"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "PolyBinary(poly_as_dict)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e77021a7",
   "metadata": {},
   "source": [
    "### PolyIsing"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c2a9688",
   "metadata": {},
   "source": [
    "Similarly, ``PolyIsing`` assumes variables $s\\in\\{-1, 1\\}$, for which we have\n",
    "\n",
    "$$ s_i^{2k + n} = s_i^n \\qquad \\forall i \\quad \\forall n,k \\in \\mathbb{N}. $$\n",
    "\n",
    "That means, we can eliminate every even exponents in this case."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10d33a83",
   "metadata": {},
   "source": [
    "Considering the above polynomial again, it reduces in the Ising format to"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "aa19a495",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+2 -1 s1 +1 s2\n"
     ]
    }
   ],
   "source": [
    "poly_ising = PolyIsing({(2, 2): 2, (2,): 1, (1,): -1})\n",
    "print(poly_ising)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36bf3b5d",
   "metadata": {},
   "source": [
    "or the example from above"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "d1fafdde",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "PolyIsing({(): 5, (('x', 1),): 3, (('y', 3),): -1, (('x', 1), ('y', 3)): -2})"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "PolyIsing(poly_as_dict)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70a30ce9",
   "metadata": {},
   "source": [
    "### Conversion from/to Ising"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b914d1f6",
   "metadata": {},
   "source": [
    "We have two conventions\n",
    "\n",
    "1. *Ising* (this convention is used by the [D-Wave API](https://docs.ocean.dwavesys.com/en/stable/)):\n",
    "    $$ x_i = \\frac{s_i + 1}{2} \\qquad s_i \\in \\{-1, 1\\}. $$\n",
    "\n",
    "2. *Inverted Ising* (this is the usual convention in gate-based quantum computing):\n",
    "    $$ x_i = \\frac{1 - z_i}{2} \\qquad z_i \\in \\{1, -1\\} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d14d3f77",
   "metadata": {},
   "source": [
    "We can convert this to an Ising model (distinguishing between both conversions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "6d567bdd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+1 -0.5 s1 +1.5 s2\n"
     ]
    }
   ],
   "source": [
    "poly_ising = poly_binary.to_ising()\n",
    "print(poly_ising)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "a17d2267",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+1 +0.5 s1 -1.5 s2\n"
     ]
    }
   ],
   "source": [
    "poly_ising_inv = poly_binary.to_ising(inverted=True)\n",
    "print(poly_ising_inv)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f96f1482",
   "metadata": {},
   "source": [
    "Surely, we can also convert the Ising models themselves into each other"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "40048f7a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly_ising.invert() == poly_ising_inv"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
