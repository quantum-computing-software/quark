import os
import nbformat


for file_name in os.listdir():
    if file_name.endswith(".ipynb"):
        print(file_name)
        with open(file_name, "r") as file:
            nb_corrupted = nbformat.reader.read(file)

        nbformat.validator.validate(nb_corrupted)

        nb_fixed = nbformat.validator.normalize(nb_corrupted)
        nbformat.validator.validate(nb_fixed[1])
        # Produces no warnings or errors.

        with open(file_name, "w") as file:
            nbformat.write(nb_fixed[1], file)
