# Installation

## via Conda (recommended)

To install quark via conda from the [DLR-SC Anaconda channel](https://anaconda.org/dlr-sc/) just run
```
conda install -c dlr-sc quark
```


## from Repository

1. Download or clone this repository.

2. We recommend to set up a virtual python environment with conda
    ```
    conda create -n quark_install python=3.11
    conda activate quark_install
    ```

3. To install the requirements of quark execute inside the top repository
    ```
    conda config --add channels conda-forge
    conda install --file ./requirements.txt
    ```

4. To install quark itself execute inside the top repository
    ```
    conda install conda-build
    conda develop ./
    ```


## Test the Installation

To test the installation run
```
python
```
and execute
```
>>> import quark
```
if this works without any output, the installation should be fine.
