#!/usr/bin/env bash

# Copyright 2024 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

################################################################################
# Run multiple scripts from the development tools
#
# Options:
#     run [setup ...] [check ...] [check_metadata ...] [create_metadata ...] [build_conda] [check_version] [create_docs ...]
################################################################################

# all possible options for this script
OPTION_SETUP="setup"
OPTION_CHECK="check"
OPTION_MD_CHECK="check_metadata"
OPTION_METADATA="create_metadata"
OPTION_DOCS="create_docs"
OPTION_VERSION="check_version"
OPTION_CONDA="build_conda"


# get and go to the repo root
REPO=$(git rev-parse --show-superproject-working-tree --show-toplevel | head -1)
cd $REPO


# check if development_tools are initialized
if [ "$(ls modules/development_tools/ -A)" ]; then
    # call the actual script passing all arguments
    echo "run script in $REPO"
else
    printf "\nERROR: Cannot run script as directory 'modules/development_tools' is empty,\n       please use 'git submodule update --init' to get submodule(s)\n\n"
    exit 0
fi

# check for positional parameters
if [ "$1" == "" ]; then
    # if no parameters are given we check the coverage and pylint
    echo "Provide exactly one of the following options: $OPTION_SETUP, $OPTION_CHECK, $OPTION_VERSION, $OPTION_CONDA, $OPTION_METADATA, $OPTION_DOCS"
else
    case "$1" in
        # check if the given parameters fit to a run option
        # and set variables accordingly
        "$OPTION_SETUP")    shift
                            echo "setup environment"
                            source modules/development_tools/environment/setup.sh "$@";;
        "$OPTION_CHECK")    shift
                            echo "run checks"
                            source modules/development_tools/checks/check.sh "$@";;
        "$OPTION_VERSION")  shift
                            echo "check version"
                            source modules/development_tools/build/check_version.sh "$@";;
        "$OPTION_CONDA")    shift
                            echo "build conda package"
                            source modules/development_tools/build/build_conda.sh "$@";;
        "$OPTION_MD_CHECK") shift
                            echo "check metadata"
                            source modules/development_tools/build/check_metadata.sh "$@";;
        "$OPTION_METADATA") shift
                            echo "create metadata"
                            source modules/development_tools/build/create_metadata.sh "$@";;
        "$OPTION_DOCS")     shift
                            echo "create documentation"
                            source modules/development_tools/docs/create_docs.sh "$@";;
        *) echo "$1 is unknown option. Allowed options: $OPTION_SETUP, $OPTION_CHECK, $OPTION_VERSION, $OPTION_CONDA, $OPTION_METADATA, $OPTION_DOCS"
    esac
fi
